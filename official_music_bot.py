# /start
    # Menu (Find by)
        # 1. by Title
        # 2. by Artist
            # Menu (Choose category : A - I, J - R, S - Z, Other)
                # 1. A - I
                # 2. J - R
                # 3. S - Z
                # 4. Other
                    # Menu (Choose a letter)
                        # 1. A
                        # 2. B
                        # 3. ...
                            # Choose a number

import telebot
import time

TOKEN = '1310248905:AAELTE0dUxFoevSgBknqlJ0uG5UaS7O13iA'
bot = telebot.TeleBot(TOKEN)

alphabet = ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', \
            'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', \
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', \
            'Other')

keyboard_main = telebot.types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
keyboard_main.add(*[telebot.types.KeyboardButton(name) for name in ['by Title', 'by Artist']])

keyboard_letters = telebot.types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
keyboard_letters.add(*[telebot.types.KeyboardButton(name) for name in ['A - I', 'J - R', 'S - Z', 'Other', '↩ to type of find..']])

keyboard_A_I = telebot.types.ReplyKeyboardMarkup(row_width=3, resize_keyboard=True)
keyboard_A_I.add(*[telebot.types.KeyboardButton(name) for name in ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', '↩ to letters..']])

keyboard_J_R = telebot.types.ReplyKeyboardMarkup(row_width=3, resize_keyboard=True)
keyboard_J_R.add(*[telebot.types.KeyboardButton(name) for name in ['J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', '↩ to letters..']])

keyboard_S_Z = telebot.types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
keyboard_S_Z.add(*[telebot.types.KeyboardButton(name) for name in ['S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '↩ to letters..']])

# page = 0
list_songs = ''
letter = ''
number = 0

@bot.message_handler(commands=['start'])
def start_message(message):
    # if page == 0:
    bot.send_message(message.chat.id, '🔎 What kind of find you need?', reply_markup=keyboard_main)
    # elif page == 1:
    #     bot.send_message(message.chat.id, 'Choose a letter!', reply_markup=keyboard2)
    # elif page == 2:
    #     bot.send_message(message.chat.id, 'Choose a letter!', reply_markup=keyboard3)

@bot.message_handler(content_types=['text'])
def choose_audio(message):
    # global page
    global list_songs
    global letter
    global number

    if message.text in alphabet:
        list_songs = ''

        f = open('./AUDIO/' + message.text + '.txt')
        letter = message.text

        while True:
            line = f.readline()
            if len(line) == 0:
                break
            # print(line, end='')
            list_songs += line

        bot.send_message(message.chat.id, list_songs)
        bot.send_message(message.chat.id, '🔢 Print number of a track!')

        f.close()
    # elif message.text == 'next': # and (page == 0 or page == 1)
    #     page += 1
    #     change_keyboard(message)
    elif message.text == 'by Artist' or message.text == 'by Title':
        # page += 1
        bot.send_message(message.chat.id, '🔤 Choose letters!', reply_markup=keyboard_letters)
    elif message.text == 'A - I' or message.text == 'J - R' or message.text == 'S - Z':
        choose_letter(message)
    elif message.text == '↩ to type of find..': # and (page == 1 or page == 2)
        # if page == 1:
        bot.send_message(message.chat.id, '🔎 What kind of find you need?', reply_markup=keyboard_main)
            # page -= 1
        # if page == 2:
    elif message.text == '↩ to letters..':
        bot.send_message(message.chat.id, '🔤 Choose letters!', reply_markup=keyboard_letters)
            # page -= 1
        # change_keyboard(message)
    elif int(message.text) > 0:
        number = int(message.text)
        send_audio(message)

    
def send_audio(message):
    # global letter
    global number
    
    if letter != '' and number != 0 and number <= int(len(open('./AUDIO/' + letter + '.txt').readlines())):
        bot.send_message(message.chat.id, '📨 Sending...')
        audio = open('./AUDIO/' + letter + '/' + str(number) + '.mp3', 'rb')
        bot.send_audio(message.chat.id, audio)

        audio.close()
        number = 0
    elif letter != '' and number > int(len(open('./AUDIO/' + letter + '.txt').readlines())):
        bot.send_message(message.chat.id, '⛔ There is no this number..')
        
        number = 0


def choose_letter(message):
    if message.text == 'A - I':
        bot.send_message(message.chat.id, '🔡 Choose a letter!', reply_markup=keyboard_A_I)
    elif message.text == 'J - R':
        bot.send_message(message.chat.id, '🔡 Choose a letter!', reply_markup=keyboard_J_R)
    elif message.text == 'S - Z':
        bot.send_message(message.chat.id, '🔡 Choose a letter!', reply_markup=keyboard_S_Z)



bot.polling()