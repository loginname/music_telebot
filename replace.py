###############################################################################
#                                  STEP #1                                    #
###############################################################################
# import os

# letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'Other', \
#     'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

# for i in letter:
#     f = open('./AUDIO/' + i + '.txt')
#     f_new = open('./AUDIO/' + i + '_new.txt', 'a')
    
#     while True:
#         line = f.readline()

#         if len(line) == 0:
#             break

#         line = line.replace('_', ' ')

#         f_new.write(line)

#     f.close()
#     f_new.close()

#     os.remove('./AUDIO/' + i + '.txt')
#     os.rename('./AUDIO/' + i + '_new.txt', './AUDIO/' + i + '.txt')
###############################################################################
#                                  STEP #2                                    #
###############################################################################
import os

letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'Other', \
    'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

for i in letter:
    f = open('./AUDIO/' + i + '.txt')

    for j in range(int(len(open('./AUDIO/' + i + '.txt').readlines()))):

        line = f.readline()
        line = line.replace(str(j+1) + '. ', '')
        line = line.replace('\n', '')

        os.rename('./AUDIO/' + i + '/' + str(j+1) + '.mp3', './AUDIO/' + i + '/' + line + '.mp3')

    f.close()